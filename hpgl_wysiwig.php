<!DOCTYPE html>
<?php

  $init = "IN;IP0,0,4000,4000;SC0,10,0,10;PU;DT.;SP1;SR5,5;PA0,0;PD;PU;";

  $cookie_name = "id";
  $cookie_old =
  $cookie_value = randomStr(10);
  if(!isset($_COOKIE[$cookie_name])) {
  setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
  }
  $output = "output/".$_COOKIE[$cookie_name]."_output.hpgl";
  if (!file_exists($output)) {
    file_put_contents($output, $init, LOCK_EX);
    chmod($output, 0777);
  }




?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>hpgl - sketchmate</title>
    <style media="screen">
        body{

        }
        span:last-of-type{
          display: none;
        }
        p{
          font-family: monospace;
          text-align: right;
        }

      .container{
        position: relative;
        display: inline-block;
        width: auto;
        height: auto;
        width: 100%;


        background: rgba(255,255,255,1);
        background: -moz-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(207,247,247,1) 100%);
        background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(100%, rgba(207,247,247,1)));
        background: -webkit-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(207,247,247,1) 100%);
        background: -o-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(207,247,247,1) 100%);
        background: -ms-linear-gradient(top, rgba(255,255,255,1) 0%, rgba(207,247,247,1) 100%);
        background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(207,247,247,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#cff7f7', GradientType=0 );

      }

      .dessin{
        position: relative;
        text-align: left;
        background: white;
        float: left;
        width: 623px;
        height: 881px;
        position: relative;
        word-wrap: break-word;
        font-size: 20px;
        border: 1px solid;
        margin-left: 4em;
        overflow: hidden;
      }
      .dessin img{
        position: absolute;
        bottom: 0;
        left:0;
      }
      .form{
        float: left;
        text-align: center;
        position: relative;
        width: 400px;
        height: auto;
      }

      textarea{
        position: relative;
        font-size: 30px;
        border: 1px solid;
      }
      input{
        position: relative;
        margin: 1em 0 0 0;
      }

    </style>
  </head>

  <body>

    <?php $image_url = $_COOKIE[$cookie_name]."_output.png" ?>
    <div class="container">
      <div class="form">
        <form action="" id="usrform" method="post">
          <textarea rows="10" cols="20" name="code" form="usrform"><?php
            $path = file_get_contents($output);
            $path = str_replace($init, "", $path);
            echo $path;
            ?></textarea><br/>
          <input type="submit" value="afficher">
        </form>
      </div>
      <div class="dessin">
        <?php

        ?>
        <?php if (file_exists("png/".$image_url)): ?>
          <img src="png/<?php echo $image_url; ?>" alt="">
        <?php endif; ?>

      </div>
    </div>

    <p>numero de session&nbsp;:&nbsp;<?php echo $_COOKIE[$cookie_name]; ?></p>
  </body>
</html>

<?php
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $chemin = $_POST["code"];
    //cheat ;PA0,0;PD;PU for keeping the proper size
    $command = $init.$chemin;

    file_put_contents($output, $command, LOCK_EX);
    chmod($output, 0777);
    header("Refresh:0");
  }
?>

<?php
//functions
function randomStr($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
