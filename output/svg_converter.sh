#!/bin/bash

while filename=$(inotifywait -q -e modify,create --format %f "../output/")
do
    extension="${filename##*.}"
    name=$(echo "$filename" | cut -f 1 -d '.')
    if [ "$extension" == "hpgl" ]
    then
      echo "modify $extension"
      hp2xx -t -m png -f ../png/$name.png $filename
    fi
done
