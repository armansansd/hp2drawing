# Hp2drawing

![](_img/out.gif)

This is a little platform that get HPGL as input and display the result as png

## Getting Started    

### Prerequisites

This tool only work on linux.
You will need to install [inotify](https://en.wikipedia.org/wiki/Inotify) and [hp2xx](https://www.gnu.org/software/hp2xx/).


## How to use

### start the daemon

Before using the platform in your browser, you have to launch the script that convert the hpgl into a png image.
Give the right permission to the file ```svg_converter.sh``` and start execute in the terminal.     

### the web interface

Clone the repo on a server or start a php server at the root of the folder.
```bash
php -S localhost:5050
```
You can then access the interface at ```localhost:5050```     

Each time you will upload HPGL code, the (above) script will generate a png to give you a preview of your drawing.

### Learn more about HPGL

https://www.isoplotec.co.jp/HPGL/eHPGL.htm#-FT(Fill%20Type)   
https://www.devenezia.com/docs/HP/index.html?1830      
http://paulbourke.net/dataformats/hpgl/   

## Author

[Bonjour Monde](http://bonjourmonde.net)

## License

[GPL](https://www.gnu.org/licenses/gpl-3.0.en.html)
