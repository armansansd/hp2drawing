<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>hpgl - sketchmate</title>
    <style media="screen">
        body{

        }
        span:last-of-type{
          display: none;
        }

      .container{
        position: relative;
        display: inline-block;
        width: auto;
        height: 100%;

        background: rgba(252,235,232,1);
        background: -moz-linear-gradient(-45deg, rgba(252,235,232,1) 0%, rgba(141,181,224,1) 100%);
        background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(252,235,232,1)), color-stop(100%, rgba(141,181,224,1)));
        background: -webkit-linear-gradient(-45deg, rgba(252,235,232,1) 0%, rgba(141,181,224,1) 100%);
        background: -o-linear-gradient(-45deg, rgba(252,235,232,1) 0%, rgba(141,181,224,1) 100%);
        background: -ms-linear-gradient(-45deg, rgba(252,235,232,1) 0%, rgba(141,181,224,1) 100%);
        background: linear-gradient(135deg, rgba(252,235,232,1) 0%, rgba(141,181,224,1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcebe8', endColorstr='#8db5e0', GradientType=1 );

        margin-left: 25%;
        margin-top: 5%;
        padding: 1em 100px 1em 100px;
        border-radius: 10px;
      }

      .info{
        background: white;
        text-align: left;
        float: left;
        width: auto;
        position: relative;
        word-wrap: break-word;
        margin: 2.3em 0 0 50px;
        font-size: 20px;
        border: 1px solid;
        padding: 1em;
      }
      .form{
        float: left;
        text-align: center;
        position: relative;
        width: 400px;
        height: auto;
        padding: 1em;
      }
      textarea{
        position: relative;
        margin: 1em 0 0 0;
        font-size: 30px;
        border: 1px solid;
      }
      input{
        position: relative;
        margin: 1em 0 0 0;
      }

    </style>
  </head>

  <body>

    <?php $output= "output/dessin.hpgl"; ?>
    <div class="container">
      <div class="form">
        <form action="" id="usrform" method="post">
          <textarea rows="10" cols="20" name="code" form="usrform"></textarea><br/>
          <input type="submit" value="Imprimer">
        </form>
      </div>
      <div class="info">
        Dernières Commandes : <br/><br/>
        <?php
        $path = file_get_contents($output);
        $path = explode(";",$path);
        foreach ($path as $key => $value) {
          echo '<span>'.$value.';</span><br/>';
        }

        ?>
      </div>
    </div>


  </body>
</html>

<?php
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $buffer = str_replace(array("\r", "\n"), '', $_POST["code"]);
    $code_array = [];
    $p = explode(";", $buffer);
    foreach ($p as $command) {
      $action = substr($command, 0,2);
      $rebuild = "";
      if($action == "PA" || $action == "PR"){
        $coord = explode(",",str_replace($action, '', $command));
        $x = $coord[0]*400;
        $y = $coord[1]*400;
        $rebuild = $action.$x.','.$y;
      }
      if( $rebuild != ""){
        array_push($code_array,$rebuild);
      }else{
        array_push($code_array,$command);
      }
    }
    $chemin = join(';', $code_array);
    $command = "IN;DT.;".$chemin;

    file_put_contents($output, $command, LOCK_EX);
    header("Refresh:0");
  }
?>
